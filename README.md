# jenkins-hello-world

Create a simple Jenkins job that runs a batch file

When viewing this README from the Jenkins job:

1) Select Configure button on left-side to view the job configuration

   
### The Jenkins Job Is Configured to:   
1) This job gets source from:
   https://esutton@bitbucket.org/esutton/jenkins-hello-world.git
2) Runs windows.bat that echos "Hello World"
3) Runs python.py that echos "Hello World"

### Starting a Jenkins Job
1) Open /jenkins/job/jenkins-hello-world/
2) On left-side, press Build Now
3) Under Build History, select the top most recent build
4) Under Build Artifacts, view environment.txt and verify it matches your PC	
5) Select Console Log to view the build log output

